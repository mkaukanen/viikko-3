
import java.util.ArrayList;
import java.util.ListIterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class Bottle {
private ArrayList<Object[]> bottleTypes = new ArrayList<Object[]>();   
 
public Bottle(){
//for (int i=0; i<6; i++){
Object[] array0= {"Pepsi Max", "0.5", "1.8"};
// if other types of bottles:
Object[] array1= {"Pepsi Max", "1.5", "2.2"};
Object[] array2= {"Coca-Cola Zero", "0.5", "2.0"};
Object[] array3= {"Coca-Cola Zero", "1.5", "2.5"};
Object[] array4= {"Fanta Zero", "0.5", "1.95"};

//fill bottle machine
bottleTypes.add(array0);
bottleTypes.add(array1);
bottleTypes.add(array2);
bottleTypes.add(array3);
bottleTypes.add(array4);
bottleTypes.add(array4);
}

    public void removeBottle(int s){
        ListIterator LI = bottleTypes.listIterator();
        for (int i=0; i<s; i++){
            LI.next();
        }
        LI.remove();   
    }
  
    public Object getBottle(int i){
        return bottleTypes.get(i)[0];
}
  
    public Object getPrice(int i){
    //price index is 2 in array
        return bottleTypes.get(i)[2];
}
    public Object getSize(int i){
        return bottleTypes.get(i)[1];
    }
    
    public int getArraySize(){
        return bottleTypes.size();
    }
   

}
