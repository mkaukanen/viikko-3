
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class BottleDispenser {
    // source used: "Olio-ohjelmointi Javalla Versio 1.0 ohjelmointiopas" by A. Herala
    // as instructed in the exercise description

    private int bottles;
    private float money;
    private float price;
    private Bottle bottleList = new Bottle();
    
    
    
    public BottleDispenser() {
        bottles = 5;
        money = 0;
    }

    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle() {
        // default bottle code;
        getInventory();
        int choice;
        Scanner scan = new Scanner(System.in);
        System.out.print("Valintasi: ");
        choice = Integer.parseInt(scan.next());
        int bottleCode = choice-1;
        price = Float.parseFloat(bottleList.getPrice(bottleCode).toString());
        if (money == 0 || money < price){
            System.out.println("Syötä rahaa ensin!");
        }
        else if (bottles == 0){
            returnMoney();
        }
        
        else{
            bottles -= 1;
            Object bottleName= bottleList.getBottle(bottleCode);
            System.out.print("KACHUNK! " + bottleName + " tipahti masiinasta!\n");
            bottleList.removeBottle(choice);
            money = (float) money - price;
                    }
    }

    
    public void returnMoney() {
        //format to right output formatting
        String rounded = String.format ("%.2f", money);
        rounded = rounded.replace(".", ",");
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "+ rounded +"€");
        money = 0;
    }  
    public void getInventory(){
        int cap;
        cap = bottleList.getArraySize();
        for (int index=0; index < cap; index++){
        String type = bottleList.getBottle(index).toString();
        String size = bottleList.getSize(index).toString();
        String price= bottleList.getPrice(index).toString();
        System.out.print(index+1 +". Nimi: " +type +"\n");
        System.out.print("\tKoko: "+ size + "\tHinta: "+ price+"\n");
        }
    }
}

